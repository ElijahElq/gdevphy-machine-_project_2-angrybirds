﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public GameManager Gamemanager;

    GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = Gamemanager.GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Bird")
        {
            gameManager.TextCondition.text = "YOU WIN SmileyFace";
            gameManager.ShowUI = true;
        }
    }
}
