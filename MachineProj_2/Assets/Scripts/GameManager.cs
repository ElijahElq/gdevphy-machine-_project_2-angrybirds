﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject UI;
    public Text TextCondition;
    public GameObject Instruction;

    Vector3 mouseClickPos;
    public bool Clicked = false;
    public bool Released = false;
    public bool ShowUI = false;

    Forces force;

    public Bird BirdProjectile = new Bird();

    // Start is called before the first frame update
    void Start()
    {
        force = GetComponent<Forces>();
        StartCoroutine(Loaded());
    }

    // Update is called once per frame
    void Update()
    {
        Mouse();

        if(Released == true)
        {
            force.ApplyLaunch(force.GravityForce);        
        }
        if (ShowUI == true)
        {
            UI.SetActive(true);
        }
    }

    void Mouse()
    {
        //Click
        if(Input.GetMouseButtonDown(0) && Clicked == false)
        {
            mouseClickPos = Input.mousePosition;
            Clicked = true;
        }

        //Release
        if(Input.GetMouseButtonUp(0) && Released == false)
        {
            Vector3 mouseReleaseDist = mouseClickPos - Input.mousePosition;
            //mouseReleaseDist.Normalize();
            Released = true;
            mouseReleaseDist.x = Mathf.Clamp(mouseReleaseDist.x, 0, 40);
            mouseReleaseDist.y = Mathf.Clamp(mouseReleaseDist.y, 0, 40);

            force.ApplyLaunch(mouseReleaseDist);
        }

        //Drag
        if (Input.GetMouseButton(0) && Clicked == true)
        {
            Vector3 mouseClickPos = Input.mousePosition;

            BirdProjectile.velocity = Vector3.zero;
            BirdProjectile.acceleration = Vector3.zero;

            for (int i = 0; i < 500; i++)
            {
                
            }
        }

    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Quit()
    {
        Application.Quit();
    }

    IEnumerator Loaded()
    {
        yield return new WaitForSeconds(0.25f);
        {
            Instruction.SetActive(true);
        }
    }
}
