﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blackhole : MonoBehaviour
{
    public GameObject Gamemanager;
    public GameObject Bird;

    private GameObject OtherObject;

    bool entered = false;

    Forces force;
    GameManager gameManager;
    Bird bird;

    // Start is called before the first frame update
    void Start()
    {
        force = Gamemanager.GetComponent<Forces>();
        gameManager = Gamemanager.GetComponent<GameManager>();
        bird = Bird.GetComponent<Bird>();
    }

    // Update is called once per frame
    void Update()
    {
        if (entered == true)
        {
            force.ApplyAttract(this.gameObject, OtherObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Bird")
        {
            gameManager.Released = false;
            entered = true;
            OtherObject = other.gameObject;
            bird.velocity = Vector3.zero;
            bird.acceleration = Vector3.zero;
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Bird")
        {
            gameManager.TextCondition.text = "You LOSE SadFace";
            gameManager.ShowUI = true;
        }
    }

}
